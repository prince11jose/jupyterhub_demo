FROM jupyterhub/jupyterhub:latest

USER apps

RUN apt-get update \
    apt-get install -y nano \
    wget
RUN mkdir /etc/jupyterhub
RUN cd /etc/jupyterhub/
RUN jupyterhub --generate-config
RUN useradd -p $(openssl passwd -crypt $Pa55w0rd) $prince
RUN echo "c.Authenticator.admin_users = {'prince11jose'}" >> /etc/jupyterhub/jupyterhub_config.py

COPY app/start_up.sh /app/start_up.sh
COPY app/gen_config.sh app/gen_config.sh 
COPY app/git_update.sh app/git_update.sh

RUN chmod +x app/start_up.sh /app/start_up.sh
RUN chmod +x app/gen_config.sh app/gen_config.sh
RUN cdmod +x app/git_update.sh app/git_update.sh

RUN echo "Demo JH"
USER $NB_UID
